export interface UserInfo {
    sub: string;
    exp: number;
    iat: number;
    username: string;
    firstName: string;
    surname: string;
    authorities: Array<Authority>
}
export interface Authority {
    authority: string
}