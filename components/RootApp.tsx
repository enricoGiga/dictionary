import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {useTypedSelector} from "../hooks/useTypedSelector";
import {NavigationContainer} from "@react-navigation/native";
import Login from "./login";
import TabNavigator from "./TabNavigator";
import Signup from "./Signup";
export type RootStackParamList = {
    Login: undefined;
    Signup: undefined;
};
const RootApp = () => {
    const Stack = createStackNavigator();
    const {isSignout, userToken} = useTypedSelector(state => state.authentication);

    return <NavigationContainer>

        {userToken == null ?

            (
                <Stack.Navigator

                    initialRouteName="Login">
                    <Stack.Screen
                        name="Login"
                        component={Login}

                        options={{
                            title: 'Sign in',
                            // When logging out, a pop animation feels intuitive
                            // You can remove this if you want the default 'push' animation
                            animationTypeForReplace: isSignout ? 'pop' : 'push',
                        }}
                    />
                    <Stack.Screen name="Signup"
                                  component={Signup}
                                  options={{
                                      title: 'Sign un',
                                      // When logging out, a pop animation feels intuitive
                                      // You can remove this if you want the default 'push' animation
                                      animationTypeForReplace: "pop" ,
                                  }}
                    />
                </Stack.Navigator>
            ) :
            (
                <TabNavigator/>
                // <Stack.Screen name="Home" component={TabNavigator}/>
            )}

    </NavigationContainer>
}
export default RootApp;