// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, {createRef, useState} from 'react';
import {
    ActivityIndicator,
    Image,
    Keyboard,
    KeyboardAvoidingView, Platform,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity, TouchableWithoutFeedback,
    View
} from "react-native";
import {UserService} from "../login/UserService";
import {StackNavigationProp} from "@react-navigation/stack";
import {RootStackParamList} from "./RootApp";
import {useNavigation} from "@react-navigation/native";


export interface SignupData {
    username: string;
    email: string;
    firstName: string;
    surname: string;
    password: string;
}

type signupScreenProp = StackNavigationProp<RootStackParamList, 'Signup'>;
const Signup: React.FC = () => {

    const signup = (user: SignupData) => {


        try {
            UserService.signup(user).then(responce => {
                setLoading(false);
                setIsSignupSuccess(true);
                console.log(responce)

            }).catch(error => {
                if (error.response.status === 400) {
                    setLoading(false);

                    console.log("Error 400 - Bad_Request");

                } else {

                    setLoading(false);
                    throw error;
                }
            })

        } catch (e) {
            console.log('errore non gestito:');
            console.log(e);


        }
    }
    const navigation = useNavigation<signupScreenProp>();
    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [surname, setSurname] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [errortext, setErrortext] = useState('');
    const [
        isSignupSuccess,
        setIsSignupSuccess
    ] = useState(false);

    const emailInputRef = createRef();
    const ageInputRef = createRef();
    const addressInputRef = createRef();
    const passwordInputRef = createRef();

    const handleSubmitButton = () => {
        setErrortext('');
        if (!userName) {
            alert('Please fill Name');
            return;
        }
        if (!email) {
            alert('Please fill Email');
            return;
        }
        if (!firstName) {
            alert('Please fill FirstName');
            return;
        }
        if (!surname) {
            alert('Please fill Surname');
            return;
        }
        if (!password) {
            alert('Please fill Password');
            return;
        }
        //Show Loader
        setLoading(true);
        const signupData: SignupData = {
            username: encodeURIComponent(userName),
            email: encodeURIComponent(email),
            firstName: encodeURIComponent(firstName),
            surname: encodeURIComponent(surname),
            password: encodeURIComponent(password),
        };
        signup(signupData)
    }

    if (isSignupSuccess) {
        return (
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#307ecc',
                    justifyContent: 'center',
                }}>
                <Image
                    source={require('../assets/fairground.svg')}
                    style={{
                        height: 150,
                        resizeMode: 'contain',
                        alignSelf: 'center'
                    }}
                />
                <Text style={styles.loginText}>
                    Registration Successful
                </Text>
                <TouchableOpacity

                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.loginText}>Login Now</Text>
                </TouchableOpacity>
            </View>
        );
    }
    return (
        <KeyboardAvoidingView
            style={styles.container}
            behavior={Platform.OS === "ios" ? "padding" : "height"}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={styles.inner}>
                    <ActivityIndicator size="large" color="#00ff00" animating={loading}/>
                    <ScrollView
                        keyboardShouldPersistTaps="handled"
                        contentContainerStyle={{
                            justifyContent: 'center',
                            alignContent: 'center',
                        }}>



                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.inputText}
                                onChangeText={(UserName) => setUserName(UserName)}

                                underlineColorAndroid="#f000"
                                placeholder="Enter Username"
                                placeholderTextColor="#cccccc"

                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.inputText}
                                onChangeText={(UserEmail) => setEmail(UserEmail)}
                                underlineColorAndroid="#f000"
                                placeholder="Enter Email"
                                placeholderTextColor="#cccccc"
                                keyboardType="email-address"
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.inputText}
                                onChangeText={(UserPassword) =>
                                    setPassword(UserPassword)
                                }
                                underlineColorAndroid="#f000"
                                placeholder="Enter Password"
                                placeholderTextColor="#cccccc"
                                secureTextEntry={true}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.inputText}
                                onChangeText={(firstName) => setFirstName(firstName)}
                                underlineColorAndroid="#f000"
                                placeholder="Enter FirstName"
                                placeholderTextColor="#cccccc"
                            />
                        </View>
                        <View style={styles.inputView}>
                            <TextInput
                                style={styles.inputText}
                                onChangeText={(surname: string) =>
                                    setSurname(surname)
                                }
                                underlineColorAndroid="#f000"
                                placeholder="Enter Surname"
                                placeholderTextColor="#cccccc"
                            />
                        </View>
                        {errortext != '' ? (
                            <Text style={styles.loginText}>
                                {errortext}
                            </Text>
                        ) : null}
                        <TouchableOpacity
                            style={styles.loginBtn}
                            activeOpacity={0.5}
                            onPress={handleSubmitButton}>
                            <Text>REGISTER</Text>
                        </TouchableOpacity>

                    </ScrollView>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
        ;
}
export default Signup;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#003f5c',

    },
    inner: {
        padding: 24,
        flex: 1,
        justifyContent: "space-around"
    },
    logo: {
        fontWeight: "bold",
        fontSize: 50,
        color: "#fb5b5a",
        marginBottom: 40
    },
    inputView: {

        backgroundColor: "#465881",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "white"
    },
    forgot: {
        color: "white",
        fontSize: 11
    },
    loginBtn: {

        backgroundColor: "#fb5b5a",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    loginText: {
        color: "white"
    }
});