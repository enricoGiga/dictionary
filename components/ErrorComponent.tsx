import {useTypedSelector} from "../hooks/useTypedSelector";
import {Text, View} from "react-native";
import React from "react";

const ErrorComponent: React.FC = () => {
    const {error} = useTypedSelector(state => state.authentication);
    if (!error)
        return null
    else {
        return <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            {console.log(error)}
            <Text style={{color: "red"}}>Errore di
                autenticazione. {error?.response?.status === 401 ? "Unauthorized" : "Unhandled error"}</Text>
        </View>
    }

}
export default ErrorComponent;