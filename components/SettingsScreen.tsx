import React from "react";
import {useActions} from "../hooks/useActions";
import {GestureResponderEvent, StyleSheet, Text, TouchableOpacity, View} from "react-native";


const SettingsScreen: React.FC = () => {
    const { logout} = useActions();
    const onPress = (event: GestureResponderEvent) => {
        event.preventDefault();
        logout();
    };



    return <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>HeyAPP</Text>
        <TouchableOpacity onPress={onPress} style={styles.logoutBtn}>
            <Text style={styles.loginText}>LOGOUT</Text>
        </TouchableOpacity>
    </View>
}
export default SettingsScreen


const styles =StyleSheet.create({
    logoutBtn: {
        width: "80%",
        backgroundColor: "#5AE3FB",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    loginText: {
        color: "white"
    }
});