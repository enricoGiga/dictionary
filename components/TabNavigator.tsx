import React, {useEffect} from "react";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Ionicons} from '@expo/vector-icons';
import HomeScreen from "./HomeScreen";
import SettingsScreen from "./SettingsScreen";
import {createStackNavigator} from "@react-navigation/stack";
import {Button, Text, View} from "react-native";


const Tab = createBottomTabNavigator();
interface LogoTitleOpt {
    title: string
}
const LogoTitle = ({title}: LogoTitleOpt) => {

        return <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>{title}</Text>
        </View>

}

const TabNavigator: React.FC = () => {
    useEffect(() => {
       console.log("use effect")
    }, []);
    return (

        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {
                    let iconName;

                    if (route.name === 'Home') {
                        iconName = focused
                            ? 'ios-information-circle'
                            : 'ios-information-circle-outline';
                    } else if (route.name === 'Settings') {
                        iconName = focused ? 'ios-list' : 'ios-list-outline';
                    }

                    // You can return any component that you like here!
                    // @ts-ignore
                    return <Ionicons name={iconName} size={size} color={color}/>;
                },
            })}
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
            }}
        >
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Settings"
                        component={SettingsStackScreen}
            />

        </Tab.Navigator>

    );
}

const HomeStack = createStackNavigator();

function HomeStackScreen() {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="StackHome" component={HomeScreen}
                              options={{
                                  headerTitle: () => <LogoTitle title="Home" />,


                              }}/>
        </HomeStack.Navigator>
    );
}


const SettingsStack = createStackNavigator();

function SettingsStackScreen() {
    return (
        <SettingsStack.Navigator>
            <SettingsStack.Screen name="StackSettings" component={SettingsScreen}
                                  options={{
                                      headerTitle: props => <LogoTitle  title="Settings" />,
                                      headerRight: () => (
                                          <Button
                                              onPress={() => alert('This is a button!')}
                                              title="Info"
                                              color="#fff"
                                          />
                                      ),
                                  }}/>
        </SettingsStack.Navigator>
    );
}

export default TabNavigator