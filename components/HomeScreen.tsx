import React from "react";
import {Text, View} from "react-native";

const HomeScreen: React.FC = () => {
    return <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Hey Home</Text>
    </View>
}
export default HomeScreen