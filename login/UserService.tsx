import axios from 'axios';
import {LoginData} from "../entities/loginData";
import {SignupData} from "../components/Signup";

export class UserService {
    static  login(user: LoginData) {
        // Simple POST request with a JSON body using axios
         return axios.post('http://192.168.1.66:8090/login', user)
    }
    static signup(user: SignupData) {
        return axios.post('http://192.168.1.66:8090/api/createUser', user)
    }
}


