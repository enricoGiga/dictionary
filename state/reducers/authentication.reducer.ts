import {LoginAction} from "../actions";
import {LoginActionType} from "../actionTypes";
import {Platform} from "react-native";
import * as SecureStore from 'expo-secure-store'
import {UserInfo} from "../../entities/UserInfo";

interface LoginState {
    isLoading: boolean
    userToken: string | null;
    isSignout: boolean;
    userInfo: UserInfo | null;
    error: any
}

let token: string = Platform.OS === 'web' ? JSON.parse(<string>localStorage.getItem('token')) : SecureStore.getItemAsync('token');
let userInfo: UserInfo = Platform.OS === 'web' ? JSON.parse(<string>localStorage.getItem('userInfo')) : SecureStore.getItemAsync('userInfo');
const initialState: LoginState = token != null ? {
    isLoading: false,
    userToken: token,
    userInfo: userInfo,
    isSignout: false,
    error: null
} : {isLoading: false, userToken: null, isSignout: true, userInfo: null, error: null}

function returnState(isLoading: boolean, isSignout: boolean, action: LoginAction): LoginState {
    return {
        isLoading: isLoading,
        userToken: action.token,
        userInfo: action.userInfo,
        isSignout: isSignout,
        error: action.error
    };
}

const authentication = (
    state: LoginState = initialState,
    action: LoginAction
): LoginState => {
    switch (action.type) {
        case LoginActionType.LOGIN_REQUEST:
            return returnState(true, true, action);
        case LoginActionType.LOGIN_SUCCESS:
            return returnState(false, false, action)
        case LoginActionType.LOGIN_FAILURE:
            return returnState(false, true, action)
        case LoginActionType.LOGOUT:
            return returnState(false, true, action)

        default:
            return state;
    }
};
export default authentication;