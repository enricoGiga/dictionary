import {LoginData} from "../../entities/loginData";
import {Dispatch} from "redux";
import {LoginAction} from "../actions";
import {LoginActionType} from "../actionTypes";
import jwt from "jwt-decode"
import {Platform} from "react-native";
import * as SecureStore from 'expo-secure-store'
import {AxiosResponse} from "axios";
import {UserService} from "../../login/UserService";
import {UserInfo} from "../../entities/UserInfo";

export const logout = () => {
    return async (dispatch: Dispatch<LoginAction>) => {
        dispatch({
            type: LoginActionType.LOGOUT,
            token: null,
            userInfo: null,
            error: null
        });
        await removeLocalStorage();
    }
}

async function saveLocalStorage(token: string, userInfo: UserInfo) {
    if (Platform.OS === 'web') {
        localStorage.setItem('token', JSON.stringify(token));
        localStorage.setItem('userInfo', JSON.stringify(userInfo));
    } else {
        await SecureStore.setItemAsync('token', JSON.stringify(token))
        await SecureStore.setItemAsync('userInfo', JSON.stringify(userInfo))
    }
}
async function  removeLocalStorage() {
    if (Platform.OS === 'web') {
        localStorage.removeItem('token');
        localStorage.removeItem('userInfo');
    } else {
        await SecureStore.deleteItemAsync('token');
        await SecureStore.deleteItemAsync('userInfo');
    }
}
export const login = (user: LoginData) => {

    return async (dispatch: Dispatch<LoginAction>) => {
        dispatch({
            type: LoginActionType.LOGIN_REQUEST,
            token: null,
            userInfo: null,
            error: null

        })
        try {       UserService.login(user).then((response: AxiosResponse) => {
                let token = response.headers.authorization;
                const user: UserInfo = jwt(token);
                saveLocalStorage(token, user);
                dispatch({
                    type: LoginActionType.LOGIN_SUCCESS,
                    token: token,
                    userInfo: user,
                    error: null
                })
            }).catch(error => {
                if (error?.response?.status === 401) {
                    console.log("token non valido");
                    removeLocalStorage();
                    dispatch({
                        type: LoginActionType.LOGIN_FAILURE,
                        token: null,
                        userInfo: null,
                        error: error
                    });
                } else {
                    throw error;
                }
            })

        } catch (error) {
            console.log('errore:');
            console.log(error);
            dispatch({
                type: LoginActionType.LOGIN_FAILURE,
                token: null,
                userInfo: null,
                error: error
            });
        }
    }
}