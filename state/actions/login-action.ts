import {LoginActionType} from "../actionTypes";
import {UserInfo} from "../../entities/UserInfo";

interface LoginRequestAction {
    type: LoginActionType;
    token: string | null;
    userInfo: UserInfo | null
    error: any
}
export type LoginAction = LoginRequestAction