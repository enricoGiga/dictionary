import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import {login, logout} from '../state';

export const useActions = () => {
  const dispatch = useDispatch();

  return bindActionCreators({login,logout}, dispatch);
};
